//
//  DataSource.swift
//  Weatherfy
//
//  Created by Eduardo Toledo on 12/13/17.
//  Copyright © 2017 SoSafe. All rights reserved.
//

import Foundation

//Se tiene un string con la información meteorológica de lluvia caida de varias ciudades, de la siguiente forma:



// Implementar la funcion `mean` que calcule el promedio de lluvia caida en la ciudad `town`, dado el string `data`.


// Implementar la funcion `variance` que calcule la varianza de lluvia caida en la ciudad `town`, dado el string `data`.
// Se puede encontrar cómo calcular la varianza en el siguiente link
// http://www.mathsisfun.com/data/standard-deviation.html
func variance(data: String, in town: String) -> Double{
    return 0.0
}

//Si la ciudad dada `town` no se encuentra en la base de datos `data`, ambas funciones deben retornar -1

//Casos de prueba:
// mean(data: data, in: "London") // 53.7(08333...)
// variance(data: data, in: "NY") // 277.7(2305...)
// mean(data: data, in: "Santiago") // -1

