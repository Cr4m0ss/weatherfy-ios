//
//  ViewController.swift
//  test
//
//  Created by Carlos Ramos on 2/7/18.
//  Copyright © 2018 Carlos Ramos. All rights reserved.
//

import UIKit
import MapKit
import Darwin

class ViewController: UIViewController {
    
    let data = ["Rome":[ 82.2, 63.2, 70.3, 53.7, 53.0, 34.4, 17.5, 27.5, 60.9, 117.7, 111.0, 97.9],
                "London":[ 44.0, 38.9, 38.9, 42.2, 47.3, 52.1, 59.5, 57.2, 55.4, 63.0, 59.0, 52.9],
                "Paris":[182.3, 120.6, 158.1, 204.9, 323.1, 300.5, 236.8, 192.9, 66.3, 63.3, 83.2, 154.7],
                "New York": [108.7, 101.8, 131.9, 93.5, 98.8, 93.6, 102.2, 131.8, 94.0, 82.3, 107.8, 94.2],
                "Vancouver":[145.7, 121.4, 102.3, 69.2, 55.8, 47.1, 31.3, 37.0, 54.6, 116.3, 154.6, 171.5],
                "Sydney":[103.4, 111.0, 131.3, 129.7, 123.0, 129.2, 102.8, 80.3, 69.3, 82.6, 81.4, 78.2],
                "Bangkok":[11.6, 28.2, 30.7, 71.8, 189.4, 151.7, 158.2, 185.0, 313.9, 230.8, 57.3, 9.4],
                "Tokyo":[49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 214.4, 194.1, 95.6, 54.4],
                "Beijing":[3.9, 4.7, 8.2, 18.4, 33.0, 78.1, 224.3, 170.0, 57.4, 18.0, 9.3, 2.7],
                "Lima":[1.3, 0.9, 0.7, 0.4, 0.7, 1.8, 4.4, 3.1, 3.3, 1.7, 0.5, 0.7]]
    
    @IBOutlet weak var varianceLabel: UILabel!
    @IBOutlet weak var averageLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func mean(data:[Double]) -> Double{
        return (data.reduce(0, +) / Double(data.count))
    }
    
    func variance(data: [Double]) -> Double{
        let mean = self.mean(data: data)
        return ((data.map{pow(($0 - mean),Double(2))}).reduce(0, +)) / Double(data.count)
    }

    
    func updateLabels(){
        self.averageLabel.text = "-1"
        self.varianceLabel.text = "-1"
    }
    
    @IBAction func search(_ sender: UIBarButtonItem) {
        let search = UISearchController(searchResultsController: nil)
        search.searchBar.delegate = self
        present(search, animated: true, completion: nil)
    }
}

extension ViewController:UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        let indicator = UIActivityIndicatorView()
        indicator.activityIndicatorViewStyle = .gray
        indicator.hidesWhenStopped = true
        indicator.startAnimating()
        self.view.addSubview(indicator)
        
        searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
        
        let request = MKLocalSearchRequest()
        request.naturalLanguageQuery = searchBar.text
        
        let active = MKLocalSearch(request: request)
        active.start { response, error in
            guard response != nil else {
                print(error ?? "Error")
                return
            }
            indicator.stopAnimating()
            let annotations = self.mapView.annotations
            self.mapView.removeAnnotations(annotations)
            UIApplication.shared.endIgnoringInteractionEvents()
            if let lat = response?.boundingRegion.center.latitude,let lon = response?.boundingRegion.center.longitude{
                
                guard let searchText = searchBar.text else {return}
                let anotation = MKPointAnnotation()
                anotation.title = searchText
                anotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                self.mapView.addAnnotation(anotation)
                
                let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                let region = MKCoordinateRegion(center: coordinate, span: span)
                self.mapView.setRegion(region, animated: true)
                
                guard let city = self.data[searchText] else {
                    self.updateLabels()
                    return
                }
                self.averageLabel.text = "\(self.mean(data: city).rounded())"
                self.varianceLabel.text = "\(self.variance(data: city).rounded())"
            }
        }
    }
}

